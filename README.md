# PayGoTest

Opcion 2: Laravel o Spring

Herramientas y librerias:

- Framework: Laravel 5.8
- Base de datos: MySQL
- Generador CRUD: [Backpack 3.6](https://backpackforlaravel.com)
- Manipulador de imagenes: [Intervention Image 2.4](http://image.intervention.io)

Instalacion:

1. Ingresar a la carpeta payGoBack.

2. Abrir el archivo .env y editar los siguientes parametros:
	- DB_HOST = 127.0.0.1
	- DB_PORT = 3306
	- DB_DATABASE = paygotest
	- DB_USERNAME = paygotest
	- DB_PASSWORD = paygotest
	- APP_URL = http://localhost.com

3. Ejecutar los siguientes comandos:
```
> composer install
> php artisan migrate
> php artisan db:seed
> php artisan backpack:base:install
> php artisan backpack:crud:install
```