<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo 'Creando la tabla cargos ' . __LINE__ . "\n";
        Schema::create('cargos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre', 64);
            $table->timestamps();
        });

        echo 'Creando la tabla departamentos ' . __LINE__ . "\n";
        Schema::create('departamentos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre', 64);
            $table->timestamps();
        });

        echo 'Creando la tabla municipios ' . __LINE__ . "\n";
        Schema::create('municipios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->unsignedBigInteger('departamento_id');
            $table->timestamps();
            $table->foreign('departamento_id')->references('id')->on('departamentos');
        });


        echo 'Creando la tabla users ' . __LINE__ . "\n";
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombres', 64);
            $table->string('primer_apellido', 64);
            $table->string('segundo_apellido', 64);
            $table->string('cedula', 16);
            $table->date('f_nacimiento');
            $table->string('genero', 1);
            $table->date('f_ingreso');
            $table->unsignedBigInteger('cargo_id');
            $table->unsignedBigInteger('jefe_id')->nullable();
            $table->string('zona', 16);
            $table->unsignedBigInteger('municipio_id');
            $table->double('ventas', 13, 2)->default(0);
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('imagen');
            $table->string('celular', 16);
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('cargo_id')->references('id')->on('cargos');
            $table->foreign('jefe_id')->references('id')->on('users');
            $table->foreign('municipio_id')->references('id')->on('municipios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        echo 'Eliminando la tabla users ' . __LINE__ . "\n";
        Schema::dropIfExists('users');

        echo 'Eliminando la tabla municipios ' . __LINE__ . "\n";
        Schema::dropIfExists('municipios');

        echo 'Eliminando la tabla departamentos ' . __LINE__ . "\n";
        Schema::dropIfExists('departamentos');

        echo 'Eliminando la tabla cargos ' . __LINE__ . "\n";
        Schema::dropIfExists('cargos');

    }
}
