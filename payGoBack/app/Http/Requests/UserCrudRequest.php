<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserCrudRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nombres' => 'required',
            'primer_apellido' => 'required',
            'segundo_apellido' => 'required',
            'cedula' => 'required|numeric',
            'f_nacimiento' => 'required|before:today',
            'f_ingreso' => 'required|before:tomorrow',
            'cargo_id' => 'required',
            'genero' => 'required',
            'zona' => 'required',
            'municipio_id' => 'required',
            'email' => 'required',
            'celular' => 'required|numeric'
        ];

        /**
         * Si se esta creando un nuevo usuario, es obligatorio la contraseña y se validan los campos unicos
         */
        if ($this->method() === 'POST') {
            $rules['password'] = 'required';
            $rules['email'] = $rules['email'] . '|unique:users,email';
            $rules['cedula'] = $rules['cedula'] . '|unique:users,cedula';
        }


        if ($this->method() === 'PUT') {
            /**
             * Se valida que el E-Mail no lo tenga otro usuario, se separa del POST para que omita el ID que
             * se esta editando
             */
            $rules['email'] = $rules['email'] . '|unique:users,email,' . $this->get('id');
            $rules['cedula'] = $rules['cedula'] . '|unique:users,cedula,' . $this->get('id');
        }

        return $rules;
    }

    /**
     * Se personalizan los mensajes de la peticion para que no se muestren en Ingles.
     * @return array
     */
    public function messages()
    {
        return [
            'nombres.required' => 'El nombre es requerido',
            'primer_apellido.required' => 'El primer apellido es requerido',
            'genero.required' => 'El genero es requerido',
            'cedula.required' => 'La cedula es requerida',
            'cedula.numeric' => 'La cedula solo debe contener números',
            'cedula.max' => 'La cedula solo debe contener 16 números como máximo',
            'cedula.unique' => 'La cedula ya esta registrada',
            'f_nacimiento.required' => 'La fecha de nacimiento es requerida',
            'f_nacimiento.before' => 'La fecha de nacimiento invalida',
            'f_ingreso.required' => 'La fecha de ingreso es requerida',
            'f_ingreso.before' => 'La fecha de ingreso invalida',
            'zona.required' => 'La zona es requerida',
            'municipio_id.required' => 'El municipio es requerido',
            'email.required' => 'El E-Mail es requerido',
            'email.unique' => 'El E-Mail ya esta registrado',
            'celular.required' => 'El celular es requerido',
            'celular.max' => 'El celular solo debe contener 16 números como máximo',
            'celular.numeric' => 'El celular solo debe contener números',
            'password.required' => 'La contraseña es requerida',
            'password.min' => 'La contraseña debe contener como mínimo 6 caracteres',
        ];
    }
}
