<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

use App\Http\Requests\UserCrudRequest as StoreRequest;
use App\Http\Requests\UserCrudRequest as UpdateRequest;
use Illuminate\Http\Request;

class UserCrudController extends CrudController
{
    /** Configuracion Backpack */
    public function setup()
    {
        $this->crud->setModel('App\Models\User');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/user');
        $this->crud->setEntityNameStrings('Usuario', 'Usuarios');

        /** Se habilitan los botones para exportar */
        $this->crud->enableExportButtons();

        /** Definición de columnas */
        $this->crud->addColumns($this->getColumns());
        $this->crud->addFields($this->getFields());
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud($request);
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud($request);
    }

    /**
     * Retorna las columnas que se mostraran en la tabla
     *
     * @return array
     */
    private function getColumns()
    {
        return [[
            'name' => 'id',
            'label' => '# Empleado',
            'type' => 'text'
        ], [
            'name' => 'nombres',
            'label' => 'Nombres',
            'type' => 'text'
        ], [
            'name' => 'primer_apellido',
            'label' => 'Primer Apellido',
            'type' => 'text'
        ], [
            'name' => 'segundo_apellido',
            'label' => 'Segundo Apellido',
            'type' => 'text'
        ], [
            'name' => 'cedula',
            'label' => 'Cedula',
            'type' => 'number'
        ], [
            'name' => 'f_nacimiento',
            'label' => 'Fecha de Nacimiento',
            'type' => 'date',
            'format' => 'D/MM/GGGG', //Para mostrar en formato 31/05/2019
        ], [
            'name' => 'genero',
            'label' => 'Genero',
            'type' => 'text'
        ], [
            'name' => 'f_ingreso',
            'label' => 'Fecha de Ingreso',
            'type' => 'date',
            'format' => 'D/MM/GGGG',
        ], [
            'label' => 'Cargo',
            'type' => 'text',
            'name' => 'cargo.nombre'
        ], [
            'label' => 'Jefe',
            'name' => 'jefe.nombre_completo',
            'type' => 'text',
        ], [
            'name' => 'zona',
            'label' => 'Zona',
            'type' => 'text'
        ], [
            'label' => 'Municipio',
            'type' => 'text',
            'name' => 'municipio.nombre'
        ], [
            'name' => 'ventas',
            'label' => 'Ventas',
            'type' => 'number',
            'prefix' => '$',
            'decimals' => 0,
        ], [
            'name' => 'email',
            'label' => 'Email',
            'type' => 'email'
        ], [
            'name' => 'imagen',
            'label' => "Imagen",
            'type' => 'image'
        ], [
            'name' => 'celular',
            'label' => 'Celular',
            'type' => 'text'
        ]];
    }

    /**
     * Retorna los campos del formulario de edicion y creacion
     *
     * @return array
     */
    private function getFields()
    {
        return [[
            'name' => 'nombres',
            'label' => 'Nombres'
        ], [
            'name' => 'primer_apellido',
            'label' => 'Primer Apellido'
        ], [
            'name' => 'segundo_apellido',
            'label' => 'Segundo Apellido'
        ], [
            'name' => 'cedula',
            'label' => 'Cedula'
        ], [
            'name' => 'f_nacimiento',
            'label' => 'Fecha de Nacimiento',
            'type' => 'date'
        ], [
            'name' => 'genero',
            'label' => 'Genero',
            'type' => 'select_from_array',
            'options' => ['M' => 'M', 'F' => 'F'],
            'allows_null' => false,
            'default' => 'M',
        ], [
            'name' => 'f_ingreso',
            'label' => 'Fecha de Ingreso',
            'type' => 'date'
        ], [
            'label' => 'Cargo',
            'type' => 'select',
            'name' => 'cargo_id',
            'entity' => 'cargo',
            'attribute' => 'nombre',
            'model' => 'App\Models\Cargo',
            'allows_null' => false,
            'options' => (function ($query) {
                //Forzar a mostrar los registros en orden ascendente
                return $query->orderBy('nombre', 'ASC')->get();
            })
        ], [
            'label' => 'Jefe',
            'type' => 'select2',
            'name' => 'jefe_id',
            'entity' => 'jefe',
            'attribute' => 'nombre_completo',
            'model' => 'App\Models\User',
            'options' => (function ($query) {
                //Forzar a mostrar los registros en orden ascendente
                return $query->orderBy('nombres', 'ASC')->get();
            })
        ], [
            'name' => 'zona',
            'label' => 'Zona',
            'allows_null' => false,
        ], [
            'label' => 'Municipio',
            'type' => 'select2',
            'name' => 'municipio_id',
            'entity' => 'municipio',
            'attribute' => 'nombre',
            'model' => 'App\Models\Municipio',
            'allows_null' => false,
            'options' => (function ($query) {
                //Forzar a mostrar los registros en orden ascendente
                return $query->orderBy('nombre', 'ASC')->get();
            }),
        ], [
            'name' => 'ventas',
            'label' => 'Ventas',
            'type' => 'number',
            'attributes' => ['step' => 'any'],
            'prefix' => '$'
        ], [
            'name' => 'email',
            'label' => 'E-Mail',
            'type' => 'email'
        ], [
            'name' => 'password',
            'label' => 'Contraseña',
            'type' => 'password'
        ], [
            'name' => 'imagen',
            'label' => 'Imagen',
            'type' => 'image',
            'upload' => true,
        ], [
            'name' => 'celular',
            'label' => 'Celular'
        ]];
    }
}
