<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Backpack\CRUD\CrudTrait;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class User extends Authenticatable
{
    use CrudTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombres', 'primer_apellido', 'segundo_apellido', 'cedula', 'f_nacimiento', 'email', 'genero',
        'password', 'f_ingreso', 'cargo_id', 'jefe_id', 'zona', 'municipio_id', 'ventas', 'imagen', 'celular'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'f_nacimiento' => 'date',
        'f_ingreso' => 'date',
        'ventas' => 'double'
    ];

    /**
     * Valores por defecto del modelo.
     *
     * @var array
     */
    protected $attributes = [
        'ventas' => 0
    ];

    public function municipio()
    {
        return $this->belongsTo(Municipio::class);
    }

    public function jefe()
    {
        /**La columna jefe_id es una llave foranea a Users, representa el jefe del usuario */
        return $this->belongsTo(User::class, 'jefe_id', 'id');
    }

    public function cargo()
    {
        return $this->belongsTo(Cargo::class);
    }

    public function subalternos()
    {
        return $this->hasMany(User::class, 'id', 'jefe_id');
    }

    /**
     * Por defecto el genero se guardara en mayuscula
     *
     * @param $value
     * @return string
     */
    public function setGeneroAttribute($value)
    {
        $this->attributes['genero'] = strtoupper($value);
    }

    /**
     * Retorna el nombre completo del usuario, nombres y apellidos concatenados.
     *
     * @return string
     */
    public function getNombreCompletoAttribute()
    {
        return $this->nombres . ' ' . $this->primer_apellido . ' ' . $this->segundo_apellido;
    }

    /**
     * Controla la forma como se almacena las imagenes de perfil.
     * Si no se envia una imagen, entonces se setea una imagen por defecto.
     * Cuando se envia una imagen el sistema la almacena en la ruta y obtiene la URL para almacenarla en la base de datos
     *
     * @param $value
     */
    public function setImagenAttribute($value)
    {
        $attribute = 'imagen';
        $nombre_archivo = 'imagen_default.png';
        $disco = 'public';
        $ruta_destino = 'usuarios/imagen_perfil/';

        //Si no envian imagen se coloca la imagen default
        if ($value == null) {
            $this->attributes[$attribute] = Storage::disk($disco)->url($ruta_destino . $nombre_archivo);
        }

        if (Str::startsWith($value, 'data:image')) {
            $imagen = Image::make($value)->encode('jpg', 90);
            $nombre_archivo = md5($value . time()) . '.jpg';
            Storage::disk($disco)->put($ruta_destino . $nombre_archivo, $imagen->stream());
            $this->attributes[$attribute] = Storage::disk($disco)->url($ruta_destino . $nombre_archivo);
        }
    }

    /**
     * Encripta la contraseña del usuario
     * @param $value
     * @return string
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }
}
